//Configuration du jeu.  Il faut penser à ajouter la configuration correspondante au fur et à mesure
let config = {
    title: 'PotterMan',
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    physics: {
        default: 'arcade',
    },
    scene: {
        preload: preload,
        create: create,
        update: update
    }
};

let game = new Phaser.Game(config);
console.log(localStorage.name);
//Premiere fonction chargée, Phaser cherche les assets
function preload ()
{
    this.load.image('background', 'assets/parchment.png');
    this.load.image('dementor', 'assets/dementor.png');
    this.load.image('coin', 'assets/coin.png');

    this.load.image('ground', 'assets/platforms.png'); //Platform horizontale
    this.load.image('ground1', 'assets/platforms1.png'); //Platform verticale
    this.load.image('ground2', 'assets/platforms2.png'); //Platform verticale petite
    this.load.image('ground3', 'assets/platforms3.png'); //Platform horizontale petite
    this.load.image('ground4', 'assets/platforms4.png'); //Platform verticale moyen
    //Le sprite est chargé comme un "sprite sheet" car il est animé
    this.load.spritesheet('perso',
        'assets/perso.png',
        { frameWidth: 32, frameHeight: 32 }
    );

    this.load.audio('music', 'assets/music/HarryPotter.mp3');
}

let player;
let monster;
let coins;
let platforms;
let platforms1;
let platforms2;
let platforms3;
let platforms4;
let score = 0;
let music;

let randomValues = [
    {x:10, y:150},
    {x:100, y:210},
    {x:278, y:398},
    {x:463, y:90},
    {x:499, y:199},
    {x:445, y:200},
    {x:752, y:200},
    {x:100, y:200},
    {x:32, y:48},
    {x:557, y:360},
    {x:635, y:246},
    {x:184, y:145},
    {x:100, y:412},
    {x:369, y:369},
    {x:765, y:150},
    {x:700, y:250},
    {x:220, y:250},
    {x:160, y:460},
    {x:120, y:420},
    {x:100, y:570},
    {x:700, y:570},
    {x:730, y:450},
    {x:320, y:550},
    {x:25, y:550},
    {x:725, y:250},
    {x:225, y:75},
    {x:480, y:200},
    {x:30, y:200},
    {x:30, y:550},
    {x:45, y:500},
    {x:45, y:500},
    {x:350, y:300},
    {x:250, y:300},
    {x:250, y:200},
    {x:270, y:120},
    {x:730, y:120},
    {x:670, y:200},
    {x:30, y:440},
    {x:430, y:540},
    {x:580, y:410},
];

function create ()
{
    this.add.image(400, 300, 'background');
    platforms = this.physics.add.staticGroup(); //Horizontal
    platforms1 = this.physics.add.staticGroup(); //Vertical
    platforms2 = this.physics.add.staticGroup(); //Vertical Petit
    platforms3 = this.physics.add.staticGroup(); //Horizontal Petit
    platforms4 = this.physics.add.staticGroup(); //Vertical Moyen

// Création des platforms horizontales
    platforms.create(660, 300, 'ground');
    platforms.create(850, 50, 'ground');
    platforms.create(-15, 250, 'ground');

    platforms3.create(425, 450, 'ground3');
    platforms3.create(635, 130, 'ground3');
    platforms3.create(45, 360, 'ground3');
    platforms3.create(140, 530, 'ground3');
    platforms3.create(140, 100, 'ground3');
    platforms3.create(340, 200, 'ground3');
    platforms3.create(470, 50, 'ground3');
    platforms3.create(570, 526, 'ground3');

//Création des platforms verticales
    platforms1.create(550, 20, 'ground1');
    platforms1.create(750, 700, 'ground1');
    platforms1.create(330, -110, 'ground1');
    platforms1.create(70, -20, 'ground1');

    platforms2.create(340, 460, 'ground2');
    platforms2.create(220, 560, 'ground2');
    platforms2.create(300, 260, 'ground2');

    platforms4.create(180, 300, 'ground4');
    platforms4.create(630, 420, 'ground4');

    //Sprite personnages
    player = this.physics.add.sprite(400, 300, 'perso');
    //monster = this.physics.add.staticGroup();

    //On permet au sprite de rebondir légèrement (indice de 0.2)
    //On fait en sorte que le sprite ne puisse pas sortir du cadre
    player.setBounce(0.2);
    player.setCollideWorldBounds(true);


    //Création des détraqueurs
    monster = this.physics.add.staticGroup();
    addMonster();


    //On génére un boucle qui permet de changer la position des détraqueurs
    let timer = this.time.addEvent({
        delay: 3000, // ms
        callback: moveMonster,
        loop: true
    });

    Getscores();

    //Création des pièces
    coins = this.physics.add.staticGroup();
    coins.create(730, 450, 'coin');
    coins.create(500, 350, 'coin');
    coins.create(590, 470, 'coin');
    coins.create(50, 300, 'coin');
    coins.create(750, 20, 'coin');
    coins.create(100, 500, 'coin');
    coins.create(300, 400, 'coin');
    coins.create(100, 50, 'coin');
    coins.create(650, 100, 'coin');
    coins.create(400, 100, 'coin');
    coins.create(400, 400, 'coin');
    coins.create(760, 400, 'coin');
    coins.create(700, 540, 'coin');
    coins.create(360, 130, 'coin');
    coins.create(20, 130, 'coin');

    //Collisions
    this.physics.add.collider(player, monster, hitMonster, null, this,);
    this.physics.add.overlap(player, coins, collectCoin, null, this);
    this.physics.add.collider(player, platforms);
    this.physics.add.collider(player, platforms1);
    this.physics.add.collider(player, platforms2);
    this.physics.add.collider(player, platforms3);
    this.physics.add.collider(player, platforms4);

    //Animation du sprite quand il va vers la gauche
    this.anims.create({
        key: 'left',
        frames: this.anims.generateFrameNumbers('perso', { start: 6, end: 11 }), //On délimite les frames utilisées pour l'animation
        frameRate: 10, //Le nombre de frame/seconde
        repeat: -1 //Signifie que l'animation doit boucler
    });
    //Animation du sprite quand il est de face (il se retourne)
    this.anims.create({
        key: 'turn',
        frames: [ { key: 'perso', frame: 1 } ],
        frameRate: 20
    });
    //Animation du sprite quand il va vers la droite
    this.anims.create({
        key: 'right',
        frames: this.anims.generateFrameNumbers('perso', { start: 12, end: 17 }),
        frameRate: 10,
        repeat: -1
    });
    //Animation du sprite quand il va vers la droite
    this.anims.create({
        key: 'up',
        frames: this.anims.generateFrameNumbers('perso', { start: 18, end: 22 }),
        frameRate: 10,
        repeat: -1
    });
    //Animation du sprite quand il va vers la droite
    this.anims.create({
        key: 'down',
        frames: this.anims.generateFrameNumbers('perso', { start: 0, end: 5 }),
        frameRate: 10,
        repeat: -1
    });

    //On ajoute le manager qui permet de contrôler le personnage avec les touches directionnelles
    cursors = this.input.keyboard.createCursorKeys();

    //Affichage du score
    let scoreText = this.add.text(620, 16, 'Gallion:',{
        fontSize: '20px', fill: 'black'
    });
    scoreText.setText('Gallions: ' + score);


    function addMonster() {
        //Affichage des détraqueurs
        for(let i = 0; i < 2; i++){

            //On choisi aléatoirement des coordonnées dans la liste
            let index = Math.floor(Math.random() * (randomValues.length - 1));
            let coord = randomValues[index];
            //Teste si deja un détraqueur
            if(coord.x && coord.y){

                let index = Math.floor(Math.random() * (randomValues.length - 1));
                let coord = randomValues[index];
                monster.create(coord.x, coord.y, 'dementor');
            }
        }
    }

    function moveMonster(){

        var dementors = monster.getChildren();
        for(let i=0; i < dementors.length; i++){
            let dementor = Phaser.Utils.Array.RemoveRandomElement(dementors);

            if (dementor)
            {
                dementor.destroy();
            }
            let index = Math.floor(Math.random() * (randomValues.length - 1));
            let coord = randomValues[index];
            //Teste si deja un détraqueur
            if(coord.x && coord.y && player){

                let index = Math.floor(Math.random() * (randomValues.length - 1));
                let coord = randomValues[index];
                monster.create(coord.x, coord.y, 'dementor');
            }
        }
    }

    // Fonction pour ramasser les pièces
    function collectCoin (player, coin) {
        coin.disableBody(true, true);
        score += 10;
        scoreText.setText('Gallions:' + score);

        if (coins.countActive(true) === 0)
        {
            //  A new batch of stars to collect
            coins.children.iterate(function (child) {
                child.enableBody(true, child.x, 0, true, true);
            });
            let x = (player.x < 400) ? Phaser.Math.Between(400, 800) : Phaser.Math.Between(0, 400);
            addMonster();
        }
    }

    //Fonction pour tuer le perso quand il touche le monstre
    function hitMonster (perso, monster){
        this.physics.pause();
        player.setTint(0x0099ff);
        player.anims.play('turn');
        let deathText = this.add.text(0, 0, "YOU DIED", {
            color:"#d53636",
            fontFamily:"Arial Black",
            fontSize:"50px"
        }).setScrollFactor(0);
        Phaser.Display.Align.In.Center(deathText, this.add.zone(400, 250, 800, 500));
        this.input.on('pointerdown', () => this.scene.restart());

        $.post("http://localhost:3000/scores", {username: localStorage.name, score: score});

        score = 0;
        music.pause();
    }

    let music = this.sound.add('music');
    music.play();
}

function SaveScore(){
    localStorage.setItem("nom", name);
    localStorage.setItem("score", score);
}


function update () {
    //Conditions pour permettre de contrôler le sprite avec les touches directionnelles
    //Courir vers la gauche
    if (cursors.left.isDown) {
        player.setVelocityX(-160);
        player.anims.play('left', true);
        //Courir vers la droite
    } else if (cursors.right.isDown) {
        player.setVelocityX(160);
        player.anims.play('right', true);
        //Aller en haut
    } else if (cursors.down.isDown) {
        player.setVelocityY(160);
        player.setVelocityX(0);
        player.anims.play('down', true);
        //Aller en bas
    } else if (cursors.up.isDown) {
        player.setVelocityY(-160);
        player.anims.play('up', true);
        //S'arrêter quand aucune touche n'est appuyée
    } else {
        player.setVelocityX(0);
        player.setVelocityY(0);
        player.anims.play('turn');
    }
}

function Getscores(){

    $.getJSON("http://localhost:3000/scores", {max : 10}, function (param) {

        param.forEach(function (data) {
            let msLi = $(document.createElement('li'));
            msLi.text(data.username + ':  ' + data.score + ' ' + 'Gallions');
            $('ul').append(msLi);
        })    });
}